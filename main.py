import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
#from google.appengine.dist import use_library
#use_library('django', '1.2')

from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app
        
class SiteMap(webapp.RequestHandler):
    def gettemplates(self, a, path, files):
        self.response.out.write('<dl>')
        if files:
            baseurl = '/'.join(path.split("/")[2:])
            self.response.out.write('<dt><h2>%s</h2></dt>' % baseurl.replace('/', ' ').replace('\\', ' '))
            for page in files:
                if page.split('.')[-1] in ['html','htm']:
                    pagename = ".".join(page.split('.')[:1])
                    url = "%s/%s" % (baseurl, pagename)
                    self.response.out.write('<dd><a href="%s" title="%s">%s</a></dd>' % (url, pagename, pagename))
        self.response.out.write('</dl>')
        
    def get(self):
        self.response.out.write('<h1>Sitemap</h1>')
        self.response.out.write('<h2>main</h2>')
        self.response.out.write(os.path.walk('./templates/', self.gettemplates, None))
        self.response.out.write('<hr /><small>GPress Light on <i>%s</i> port %s</small>' % (os.environ['HTTP_HOST'].split(':')[0], os.environ['SERVER_PORT']))

class SiteMapXml(webapp.RequestHandler):
    def gettemplates(self, a, path, files):
        # Recover pages
        self.response.out.write('')
        
    def get(self):
        # Build the XML and send it to the browser
        self.response.out.write('')


class SoundXml(webapp.RequestHandler):
    def get(self):
        self.response.out.write(template.render('templates/sound-config.xml', {}))


class Google(webapp.RequestHandler):
    def get(self, id):
        self.response.out.write('google-site-verification: google'+id+'.html')

class MainPage(webapp.RequestHandler):
    def notfound(self):
        self.response.out.write('<h1>404 - <i>Page not found</i>.</h1><p>Try searching on <a href="/gpe-sitemap" title="Sitemap">this page</a>.</p>')
        self.response.out.write("""<hr /><pre>    It is said, "To err is human"
    That quote from alt.times.lore,
    Alas, you have made an error,
    So I say, "404."

    Double-check your URL,
    As we all have heard before.
    You ask for an invalid filename,
    And I respond, "404."

    Perhaps you made a typo --
    Your fingers may be sore --
    But until you type it right,
    You'll only get 404.

    Maybe you followed a bad link,
    Surfing a foreign shore;
    You'll just have to tell that author
    About this 404.

    I'm just a lowly server
    (Who likes to speak in metaphor),
    So for a request that Idon't know,
    I must return 404.

    Be glad I'm not an old mainframe
    That might just dump its core,
    Because then you'd get a ten-meg file
    Instead of this 404.

    I really would like to help you,
    But I don't know what you're looking for,
    And since I don't know what you want,
    I give you 404.

    Remember Poe, insane with longing
    For his tragically lost Lenore.
    Instead, you quest for files.
    Quoth the Raven, "404!"
        </pre>""")
        self.response.out.write('<hr /><small>GPress Light on <i>%s</i> port %s</small>' % (os.environ['HTTP_HOST'].split(':')[0], os.environ['SERVER_PORT']))

    def get(self):
        host = os.environ["HTTP_HOST"].split('.')
        host.reverse()
        if not Debug and host[1] != "appspot":
            self.response.out.write(template.render('%s/templates/courtesy.html' % os.path.curdir, {}))
            return
        request = self.request.path if self.request.path != '/' else '/index'
        reqpath = '%s/templates%s' % (os.path.curdir, request if request.split('.')[-1] in ['html','htm'] else (('%s.htm' % request) if os.path.exists(('%s/templates%s.htm' % (os.path.curdir, request))) else '%s.html' % request))
        if not os.path.exists(reqpath):
            self.error(404)
            self.notfound()
            return
        
        self.response.out.write(template.render(reqpath, {}))

Debug = os.environ["SERVER_SOFTWARE"].split("/")[0].lower() == "development"

application = webapp.WSGIApplication([
                  ('/sitemap', SiteMap),
                  ('/google(.*)\.html', Google),
                  ('/sitemap.xml', SiteMapXml),
                  ('/sound-config.xml', SoundXml),
                  ('.*', MainPage)],
                  debug=Debug)

if __name__ == "__main__":
    run_wsgi_app(application)